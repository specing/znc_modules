/*
 * Copyright (C) 2008-2012  See the AUTHORS file for details.
 * Copyright (C) 2006-2007, CNU <bshalm@broadpark.no> (http://cnu.dieplz.net/znc)
 * Copyright (C) 2017-2020, Fedja Beader <fedja@protonmail.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */

#include <znc/FileUtils.h>
#include <znc/User.h>
#include <znc/IRCNetwork.h>
#include <znc/Chan.h>
#include <znc/Server.h>

#define BUFFSIZE 512

using std::vector;

class CLogMod: public CModule {
public:
	MODCONSTRUCTOR(CLogMod) {}

	void PutLog(const CString& sLine, const CString& sWindow = "status");
	void PutLog(const CString& sLine, const CChan& Channel);
	void PutLog(const CString& sLine, const CNick& Nick);
	CString GetServer();

	virtual bool OnLoad(const CString& sArgs, CString& sMessage);
	virtual void OnIRCConnected();
	virtual void OnIRCDisconnected();
	virtual EModRet OnBroadcast(CString& sMessage);

	virtual void OnRawMode2(const CNick* pOpNick, CChan& Channel, const CString& sModes, const CString& sArgs);
	virtual void OnKick(const CNick& OpNick, const CString& sKickedNick, CChan& Channel, const CString& sMessage);
	virtual void OnQuit(const CNick& Nick, const CString& sMessage, const vector<CChan*>& vChans);
	virtual void OnJoin(const CNick& Nick, CChan& Channel);
	virtual void OnPart(const CNick& Nick, CChan& Channel, const CString& sMessage);
	virtual void OnNick(const CNick& OldNick, const CString& sNewNick, const vector<CChan*>& vChans);
	virtual EModRet OnTopic(CNick& Nick, CChan& Channel, CString& sTopic);

	/* notices */
	virtual EModRet OnUserNotice(CString& sTarget, CString& sMessage);
	virtual EModRet OnPrivNotice(CNick& Nick, CString& sMessage);
	virtual EModRet OnChanNotice(CNick& Nick, CChan& Channel, CString& sMessage);

	/* actions */
	virtual EModRet OnUserAction(CString& sTarget, CString& sMessage);
	virtual EModRet OnPrivAction(CNick& Nick, CString& sMessage);
	virtual EModRet OnChanAction(CNick& Nick, CChan& Channel, CString& sMessage);

	/* msgs */
	virtual EModRet OnUserMsg(CString& sTarget, CString& sMessage);
	virtual EModRet OnPrivMsg(CNick& Nick, CString& sMessage);
	virtual EModRet OnChanMsg(CNick& Nick, CChan& Channel, CString& sMessage);

private:
	class m_BufferedLog {
	public:
		m_BufferedLog ():m_size(0) {}
		// When things start falling apart, we pack our bags and run away screaming
		~m_BufferedLog () { flush("destruct"); }

		void append (const CLogMod& parent, const CString& sLine, const CString& sWindow);
		/* Should only be called when:
		 * a) buffer is full
		 * b) the log path has been changed (rotated)
		 */
		void flush (const char* reason);
	private:
		char	m_buffer[BUFFSIZE];
		size_t	m_size;
		CString	m_path;
	};

	std::map<CString, m_BufferedLog> m_logs;
	CString                          m_sLogPath;
};

void CLogMod::m_BufferedLog::append(const CLogMod& parent, const CString& sLine, const CString& sWindow)
{
	CString sPath;
	time_t curtime;

	time(&curtime);
	// Generate file name
//	sPath = CUtils::FormatTime(curtime, parent.m_sLogPath, parent.m_pUser->GetTimezone());
	sPath = CUtils::FormatTime(curtime, parent.m_sLogPath, "UTC");
	if (sPath.empty())
	{
//		DEBUG("Could not format log path [" << sPath << "]");
		return;
	}

	// $WINDOW has to be handled last, since it can contain %
	sPath.Replace("$NETWORK", (parent.m_pNetwork ? parent.m_pNetwork->GetName() : "znc"));
	sPath.Replace("$WINDOW", sWindow.Replace_n("/", "?"));
	sPath.Replace("$USER", (parent.m_pUser ? parent.m_pUser->GetUserName() : "UNKNOWN"));

	// Check if it's allowed to write in this specific path
	sPath = CDir::CheckPathPrefix(parent.GetSavePath(), sPath);
	if (sPath.empty())
	{
//		DEBUG("Invalid log path [" << parent.m_sLogPath << "].");
		return;
	}

	// Check if we have to rotate...
	if (sPath != m_path)
	{
		flush("rotate");
		m_path = sPath;
	}


	CString line = CUtils::FormatTime(curtime, "[%H:%M:%S] ", parent.m_pUser->GetTimezone()) + sLine + "\n";

//	DEBUG("logging: (" << line.size() << ") [" << line << "]");

	if (m_size + line.size() > BUFFSIZE)
	{
		size_t first_part  = BUFFSIZE - m_size;
		size_t second_part = line.size() - first_part;
		// copy the first part and flush
		line.copy (m_buffer + m_size, first_part, 0);
		m_size = BUFFSIZE;

		flush("block");

		// copy the remaining part
		line.copy (m_buffer, second_part, first_part);
		m_size = second_part;
	}
	else
	{
		line.copy (m_buffer + m_size, line.size());
		m_size += line.size();
	}
}

void CLogMod::m_BufferedLog::flush(const char * reason)
{
//	DEBUG("flushing because " << reason << ": [" << m_path << "]");

	if (m_path.empty())
		return;

	CFile LogFile(m_path);
	CString sLogDir = LogFile.GetDir();

	if (!CFile::Exists(sLogDir))
		CDir::MakeDir(sLogDir);


	if (!LogFile.Open(O_WRONLY | O_APPEND | O_CREAT))
	{
//		DEBUG("Could not open log file [" << m_path << "]: " << strerror(errno));
		return;
	}

	size_t current_size = LogFile.GetSize();
	size_t have = current_size % BUFFSIZE;
//	DEBUG("current size: " << current_size << " m_size: " << m_size);

	if (have)
	{ // we have unaligned logs, align them
		size_t need = BUFFSIZE - have;

		size_t to_write = std::min (need, m_size);

		LogFile.Write (m_buffer, to_write);

		memmove (m_buffer, m_buffer + to_write, (BUFFSIZE - to_write));

		m_size = m_size - to_write;
	}
	else
	{
		LogFile.Write(m_buffer, m_size);
		m_size = 0;
	}
}

void CLogMod::PutLog(const CString& sLine, const CString& sWindow /*= "Status"*/)
{
	m_logs [sWindow].append (*this, sLine, sWindow);
}

void CLogMod::PutLog(const CString& sLine, const CChan& Channel)
{
	PutLog(sLine, Channel.GetName());
}

void CLogMod::PutLog(const CString& sLine, const CNick& Nick)
{
	PutLog(sLine, Nick.GetNick());
}

CString CLogMod::GetServer()
{
	CServer* pServer = m_pNetwork->GetCurrentServer();
	CString sSSL;

	if (!pServer)
		return "(no server)";

	if (pServer->IsSSL())
		sSSL = "+";
	return pServer->GetName() + " " + sSSL + CString(pServer->GetPort());
}

bool CLogMod::OnLoad(const CString& sArgs, CString& sMessage)
{
	// Use load parameter as save path
	m_sLogPath = sArgs;

	// Add default filename to path if it's a folder
	if (GetType() == CModInfo::UserModule) {
		if (m_sLogPath.Right(1) == "/" || m_sLogPath.find("$WINDOW") == CString::npos || m_sLogPath.find("$NETWORK") == CString::npos) {
			if (!m_sLogPath.empty()) {
				m_sLogPath += "/";
			}
			m_sLogPath += "$NETWORK_$WINDOW_%Y%m%d.log";
		}
	} else if (GetType() == CModInfo::NetworkModule) {
		if (m_sLogPath.Right(1) == "/" || m_sLogPath.find("$WINDOW") == CString::npos) {
			if (!m_sLogPath.empty()) {
				m_sLogPath += "/";
			}
			m_sLogPath += "$WINDOW_%Y%m%d.log";
		}
	} else {
		if (m_sLogPath.Right(1) == "/" || m_sLogPath.find("$USER") == CString::npos || m_sLogPath.find("$WINDOW") == CString::npos || m_sLogPath.find("$NETWORK") == CString::npos) {
			if (!m_sLogPath.empty()) {
				m_sLogPath += "/";
			}
			m_sLogPath += "$USER_$NETWORK_$WINDOW_%Y%m%d.log";
		}
	}

	// Check if it's allowed to write in this path in general
	m_sLogPath = CDir::CheckPathPrefix(GetSavePath(), m_sLogPath);
	if (m_sLogPath.empty())
	{
		sMessage = "Invalid log path ["+m_sLogPath+"].";
		return false;
	} else {
		sMessage = "Logging to ["+m_sLogPath+"].";
		return true;
	}
}


void CLogMod::OnIRCConnected()
{
	PutLog("Connected to IRC (" + GetServer() + ")");
}

void CLogMod::OnIRCDisconnected()
{
	PutLog("Disconnected from IRC (" + GetServer() + ")");
}

CModule::EModRet CLogMod::OnBroadcast(CString& sMessage)
{
	PutLog("Broadcast: " + sMessage);
	return CONTINUE;
}

void CLogMod::OnRawMode2(const CNick* pOpNick, CChan& Channel, const CString& sModes, const CString& sArgs)
{
	if (pOpNick)
		PutLog("*** " + pOpNick->GetNick() + " sets mode: " + sModes + " " + sArgs, Channel);
	else
		PutLog("*** Server sets mode: " + sModes + " " + sArgs, Channel);
}

void CLogMod::OnKick(const CNick& OpNick, const CString& sKickedNick, CChan& Channel, const CString& sMessage)
{
	PutLog("*** " + sKickedNick + " was kicked by " + OpNick.GetNick() + " (" + sMessage + ")", Channel);

	// we were kicked, flush logs...
	if (m_pNetwork->GetCurNick().Equals(sKickedNick)) {
		m_logs.erase (Channel.GetName());
	}
}

void CLogMod::OnQuit(const CNick& Nick, const CString& sMessage, const vector<CChan*>& vChans)
{
	for (std::vector<CChan*>::const_iterator pChan = vChans.begin(); pChan != vChans.end(); ++pChan)
		PutLog("*** Quits: " + Nick.GetNick() + " (" + Nick.GetIdent() + "@" + Nick.GetHost() + ") (" + sMessage + ")", **pChan);
}

void CLogMod::OnJoin(const CNick& Nick, CChan& Channel)
{
	PutLog("*** Joins: " + Nick.GetNick() + " (" + Nick.GetIdent() + "@" + Nick.GetHost() + ")", Channel);
}

void CLogMod::OnPart(const CNick& Nick, CChan& Channel, const CString& sMessage)
{
	PutLog("*** Parts: " + Nick.GetNick() + " (" + Nick.GetIdent() + "@" + Nick.GetHost() + ") (" + sMessage + ")", Channel);

	// we don't want any more of their garbage taking our precious ram,...
	if (m_pNetwork->GetCurNick().Equals(Nick.GetNick()))
		m_logs.erase (Channel.GetName());
}

void CLogMod::OnNick(const CNick& OldNick, const CString& sNewNick, const vector<CChan*>& vChans)
{
	for (std::vector<CChan*>::const_iterator pChan = vChans.begin(); pChan != vChans.end(); ++pChan)
		PutLog("*** " + OldNick.GetNick() + " is now known as " + sNewNick, **pChan);
}

CModule::EModRet CLogMod::OnTopic(CNick& Nick, CChan& Channel, CString& sTopic)
{
	PutLog("*** " + Nick.GetNick() + " changes topic to '" + sTopic + "'", Channel);
	return CONTINUE;
}

/* notices */
CModule::EModRet CLogMod::OnUserNotice(CString& sTarget, CString& sMessage)
{
	if (m_pNetwork) {
		PutLog("-" + m_pNetwork->GetCurNick() + "- " + sMessage, sTarget);
	}

	return CONTINUE;
}

CModule::EModRet CLogMod::OnPrivNotice(CNick& Nick, CString& sMessage)
{
	PutLog("-" + Nick.GetNick() + "- " + sMessage, Nick);
	return CONTINUE;
}

CModule::EModRet CLogMod::OnChanNotice(CNick& Nick, CChan& Channel, CString& sMessage)
{
	PutLog("-" + Nick.GetNick() + "- " + sMessage, Channel);
	return CONTINUE;
}

/* actions */
CModule::EModRet CLogMod::OnUserAction(CString& sTarget, CString& sMessage)
{
	if (m_pNetwork) {
		PutLog("* " + m_pNetwork->GetCurNick() + " " + sMessage, sTarget);
	}

	return CONTINUE;
}

CModule::EModRet CLogMod::OnPrivAction(CNick& Nick, CString& sMessage)
{
	PutLog("* " + Nick.GetNick() + " " + sMessage, Nick);
	return CONTINUE;
}

CModule::EModRet CLogMod::OnChanAction(CNick& Nick, CChan& Channel, CString& sMessage)
{
	PutLog("* " + Nick.GetNick() + " " + sMessage, Channel);
	return CONTINUE;
}

/* msgs */
CModule::EModRet CLogMod::OnUserMsg(CString& sTarget, CString& sMessage)
{
	if (m_pNetwork) {
		PutLog("<" + m_pNetwork->GetCurNick() + "> " + sMessage, sTarget);
	}

	return CONTINUE;
}

CModule::EModRet CLogMod::OnPrivMsg(CNick& Nick, CString& sMessage)
{
	PutLog("<" + Nick.GetNick() + "> " + sMessage, Nick);
	return CONTINUE;
}

CModule::EModRet CLogMod::OnChanMsg(CNick& Nick, CChan& Channel, CString& sMessage)
{
	PutLog("<" + Nick.GetNick() + "> " + sMessage, Channel);
	return CONTINUE;
}

template<> void TModInfo<CLogMod>(CModInfo& Info) {
	Info.AddType(CModInfo::NetworkModule);
	Info.AddType(CModInfo::GlobalModule);
	Info.SetHasArgs(true);
	Info.SetArgsHelpText("Optional path where to store logs.");
	Info.SetWikiPage("log");
}

USERMODULEDEFS(CLogMod, "Write (buffered) IRC logs")
